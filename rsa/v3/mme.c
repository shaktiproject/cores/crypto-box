#include<stdio.h>

typedef unsigned char BYTE;             // 8-bit byte
typedef unsigned int  WORD;             // 32-bit word, change to "long" for 16-bit machines

WORD golden_result(WORD ai, WORD e, WORD m) {
  unsigned long long a= ai;
  //printf("C: Input: a: %lld e: %d m: %d \n",a,e,m);
  WORD res = 1;      // Initialize result
  a = a % m;  

  while (e > 0)
  {
    // If e is odd, multiply a with result
    if(e & 1)
      res = (res*a) % m;

    e = e>>1;
    a = (a*a) % m;
    //printf("a: %d\n",a);
  }
  //printf("C: Result. Res: %d\n", res);
  return res;
}

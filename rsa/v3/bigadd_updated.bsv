package bigadd_updated;

import Vector ::*;

interface Ifc_bigadd#(numeric type n);
  method ActionValue#(Bit#(TAdd#(n,1))) addn(Bit#(n) a, Bit#(n) b, Bit#(1) carryin);
endinterface

module mkBigAdd(Ifc_bigadd#(n)); 
  method ActionValue#(Bit#(TAdd#(n,1))) addn(Bit#(n) a, Bit#(n) b, Bit#(1) carryin);
    return zeroExtend(a)+zeroExtend(b)+zeroExtend(carryin);
  endmethod
endmodule

module mkTb(Empty);
    Ifc_bigadd#(64) ba_mod <- mkBigAdd;
    rule rl_start;
      let out <- ba_mod.addn(64'd99999, 64'd88882, 1'b0);
      $display("%d", out);
      $finish(0);
    endrule
endmodule



endpackage


package mme_ui;
  import mme :: *;
  import AXI4_Lite_Types::*;
  import AXI4_Lite_Fabric::*;
  import AXI4_Types::*;
  import AXI4_Fabric::*;
  import Semi_FIFOF::*;
  import GetPut::*;
  import FIFO::*;
  import Vector ::*;
  import Clocks::*;
  import BUtils::*;
  import device_common::*;
  
  interface UserInterface#(numeric type addr_width, numeric type data_width, numeric type total_width);
  	method ActionValue#(Tuple2#(Bit#(data_width),Bool)) read_req (Bit#(addr_width) addr, AccessSize size);
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(data_width) data);
  	method Bool can_take_req;
  endinterface
  
  /*
  `define InputText 'b00;
  `define ExpText 'b20;
  `define ModText 'b40;
  `define RsqrModNText 'b60;
  `define OutputReg 'h80;
  `define StatusReg 'hc0;
  */
  `include "mme.defines"
  typedef enum {
  	Idle, Compute, Ready
  } MME_State_type deriving(Bits, Eq, FShow);
  
  module mkuser_mme(UserInterface#(addr_width, data_width, total_width))
  provisos(Add#(a__, 8, addr_width), Add#(b__, 64, total_width), Add#(c__, TLog#(total_width), total_width), Add#(e__, total_width, TMul#(2, total_width)), Add#(d__, TLog#(TAdd#(1, total_width)), TAdd#(TLog#(total_width), 1)), Mul#(f__, 256, TAdd#(total_width, 256)), Mul#(g__, 256, total_width), Mul#(num_vec, data_width, total_width), Add#(h__, 1, data_width));

  	Ifc_mme#(total_width,256) mmeMod <- mkMME;

  	Reg#(Bit#(total_width)) mme_out <- mkReg(0);
  	Reg#(Bit#(total_width)) rg_input_text <- mkReg(0);
  	Reg#(Bit#(total_width)) rg_exp_text <- mkReg(0);
  	Reg#(Bit#(total_width)) rg_mod_text <- mkReg(0);
  	Reg#(Bit#(total_width)) rg_rsqrmodn <- mkReg(0);

    Vector#(num_vec, Reg#(Bit#(data_width))) vec_inp <- replicateM(mkReg(0));
    Vector#(num_vec, Reg#(Bit#(data_width))) vec_exp <- replicateM(mkReg(0));
    Vector#(num_vec, Reg#(Bit#(data_width))) vec_mod <- replicateM(mkReg(0));
    Vector#(num_vec, Reg#(Bit#(data_width))) vec_rsq <- replicateM(mkReg(0));

  	Reg#(Bool) rg_inpR <- mkReg(False);
  	Reg#(Bool) rg_expR <- mkReg(False);
  	Reg#(Bool) rg_modR <- mkReg(False);
  	Reg#(Bool) rg_rsqR <- mkReg(False);
  	Reg#(Bool) opReady <- mkReg(False);
  
  	Reg#(Bit#(TLog#(total_width))) inp_index <- mkReg(0);
  	Reg#(Bit#(TLog#(total_width))) exp_index <- mkReg(0);
  	Reg#(Bit#(TLog#(total_width))) mod_index <- mkReg(0);
  	Reg#(Bit#(TLog#(total_width))) rsq_index <- mkReg(0);
  	Reg#(Bit#(TLog#(total_width))) out_index <- mkReg(0);
  	//Reg#(Bit#(TAdd#(TLog(total_width),1))) inp_index <- mkReg(0);
  	//Reg#(Bit#(TAdd#(TLog(total_width),1))) exp_index <- mkReg(0);
  	//Reg#(Bit#(TAdd#(TLog(total_width),1))) mod_index <- mkReg(0);
  	//Reg#(Bit#(TAdd#(TLog(total_width),1))) rsq_index <- mkReg(0);
  	//Reg#(Bit#(TAdd#(TLog(total_width),1))) output_index <- mkReg(0);
  
  	rule rl_start(mmeMod.isReady && rg_inpR && rg_expR && rg_modR && rg_rsqR);
  		mmeMod.mmeExp(pack(readVReg(vec_inp)),pack(readVReg(vec_exp)),pack(readVReg(vec_mod)),pack(readVReg(vec_rsq)));
  		//mmeMod.mmeExp(rg_input_text, rg_exp_text, rg_mod_text, rg_rsqrmodn);
      rg_inpR <= False;
      rg_expR <= False;
      rg_modR <= False;
      rg_rsqR <= False;
      opReady <= False;
      $display("MME: Rule Start");

  	endrule
  	rule rl_getOutput((mmeMod.isReady) && (!opReady));
  		mme_out <= mmeMod.getResult();	
      opReady <= True;
      $display("MME: GetOutput %h", mmeMod.getResult());
  	endrule
  	
  	method ActionValue#(Tuple2#(Bit#(data_width),Bool)) read_req (Bit#(addr_width) addr, AccessSize size) if((mmeMod.isReady) && (opReady));
  		if(truncate(addr) == `OutputReg) begin
        $display("MME: Start Read Req %h, %h", mme_out, out_index);
  			//out_index<= ~out_index;
  			//if(out_index==0)
        if(inp_index!=fromInteger(valueOf(total_width)/valueOf(data_width) -1)) begin
          out_index <= out_index+1;
  				return tuple2(mme_out[fromInteger(valueOf(data_width))*(out_index+1)-1:fromInteger(valueOf(data_width))*out_index], True);
        end
        else begin
          opReady <= False;
          out_index<=0;
  				//return tuple2(mme_out[127:64], True);
  				return tuple2(mme_out[fromInteger(valueOf(data_width))*(out_index+1)-1:fromInteger(valueOf(data_width))*out_index], True);
        end
        //if(inp_index==fromInteger(valueOf(total_width)/64)) begin
  			//  output_index<= 0;
        //end
        //return tuple2(mme_out[64*(out_index+1)-1:64*out_index],True)
  		end
  		else if(truncate(addr) == `StatusReg)
  			return tuple2(zeroExtend(pack(mmeMod.isReady)), True);
  		else
  			return tuple2(?, False);
  	endmethod
  
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(data_width) data) if(mmeMod.isReady);
      $display("MME: Start Write Req %h, %h", data, addr);
  		Bool lv_success= True;
  		if(truncate(addr) == `InputText) begin
  			//inp_index<= ~inp_index;
        if(inp_index!=fromInteger(valueOf(total_width)/valueOf(data_width) -1)) begin
  				//rg_input_text[63:0] <= data;
          vec_inp[inp_index] <= data;
          inp_index <= inp_index+1;
        end
        else begin
  				//rg_input_text[127:64] <= data;
          vec_inp[inp_index] <= data;
          rg_inpR<=True;
          inp_index <= 0;
        end
        //rg_input_text[64*inp_index:+63] <= data;
        //if(inp_index==fromInteger(valueOf(total_width)/64)) begin
        //  rg_ipR <= True;
  			//  inp_index<= 0;
        //end
  		end
      else if(truncate(addr) == `ExpText) begin
  			//exp_index<= ~exp_index;
  			//if(exp_index==0)
        if(exp_index!=fromInteger(valueOf(total_width)/valueOf(data_width) -1)) begin
  				//rg_exp_text[63:0] <= data;
          vec_exp[exp_index] <= data;
          exp_index <= exp_index+1;
        end
        else begin
  				//rg_exp_text[127:64] <= data;
          vec_exp[exp_index] <= data;
          rg_expR <= True;
          exp_index<=0;
        end
        //rg_exp_text[64*exp_index:+63] <= data;
        //if(exp_index==fromInteger(valueOf(total_width)/64)) begin
        //  rg_expR <= True;
  			//  exp_index<= 0;
        //end
  		end
      else if(truncate(addr) == `ModText) begin
  			//mod_index<= ~mod_index;
        //if(mod_index==0) begin
        if(mod_index!=fromInteger(valueOf(total_width)/valueOf(data_width) -1)) begin
  				//rg_mod_text[63:0] <= data;
          vec_mod[mod_index] <= data;
          mod_index <= mod_index+1;
        end
        else begin
  				//rg_mod_text[127:64] <= data;
          vec_mod[mod_index] <= data;
          mod_index <= 0;
          rg_modR <= True;
        end
        //rg_mod_text[64*mod_index:+63] <= data;
        //if(mod_index==fromInteger(valueOf(total_width)/64)) begin
        //  rg_modR <= True;
  			//  mod_index<= 0;
        //end
  		end
      else if(truncate(addr) == `RsqrModNText) begin
  			//rsq_index<= ~rsq_index;
        //if(rsq_index==0) begin
        if(rsq_index!=fromInteger(valueOf(total_width)/valueOf(data_width) -1)) begin
  				//rg_rsqrmodn[63:0] <= data;
          vec_rsq[rsq_index] <= data;
          rsq_index <= rsq_index+1;
        end
        else begin
  				//rg_rsqrmodn[127:64] <= data;
          vec_rsq[rsq_index] <= data;
          rsq_index <=0;
          rg_rsqR<= True;
        end
        //rg_rsqrmodn[64*rsq_index:+63] <= data;
        //if(rsq_index==fromInteger(valueOf(total_width)/64)) begin
        //  rg_rsqR <= True;
  			//  rsq_index<= 0;
        //end
  		end
  		else
  			lv_success= False;
  
  		return lv_success;
  	endmethod
  
  	method Bool can_take_req;
  		return mmeMod.isReady;
  	endmethod
  endmodule
  
  interface Ifc_mme_axi4lite#(numeric type addr_width, 
                              numeric type data_width, 
                              numeric type user_width);
  	interface AXI4_Lite_Slave_IFC#(addr_width, data_width, user_width) slave; 
    method Bool can_take_inp;
    method Bool outp_ready;
     //interface RS264 io;
 	endinterface
  
  module mkmme_axi4lite#(Clock mme_clock, Reset mme_reset)
  															(Ifc_mme_axi4lite#(addr_width,data_width,user_width))
  // same provisos for the aes
      provisos( Add#(data_width,0,64),
                Add#(a__, 8, addr_width),
                Add#(h__, 1, data_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=mme_clock);
  	AXI4_Lite_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Lite_Slave_Xactor();
  
  	if(!sync_required)begin // If aes is clocked by core-clock.
  		UserInterface#(addr_width, data_width, 2048) user_ifc<- mkuser_mme();
  		//UserInterface#(addr_width) user_ifc<- mkuser_aes(clocked_by mme_clock, 
      //                                                             reset_by mme_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- user_ifc.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- user_ifc.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
      		s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
      method Bool can_take_inp = user_ifc.can_take_req;
      method Bool outp_ready = user_ifc.can_take_req;
  		//interface io= user_ifc.io;
  	end
  	else begin // if core clock and mme_clock is different.
  		UserInterface#(addr_width, data_width, 2048) user_ifc<- mkuser_mme;
  		SyncFIFOIfc#(AXI4_Lite_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,mme_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,mme_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,mme_clock);
  		SyncFIFOIfc#(AXI4_Lite_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  															mkSyncFIFOToCC(3,mme_clock,mme_reset);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,mme_clock,mme_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- user_ifc.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- user_ifc.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
      method Bool can_take_inp = user_ifc.can_take_req;
      method Bool outp_ready = user_ifc.can_take_req;
  		//interface io= user_ifc.io;
  	end
  endmodule:mkmme_axi4lite

interface Ifc_mme_axi4#(numeric type addr_width, 
                              numeric type data_width, 
                              numeric type user_width);
  	interface AXI4_Slave_IFC#(addr_width, data_width, user_width) slave; 
     //interface RS264 io;
    method Bool can_take_inp;
    method Bool outp_ready;
 	endinterface
  
  module mkmme_axi4#(Clock mme_clock, Reset mme_reset)
  															(Ifc_mme_axi4#(addr_width,data_width,user_width))
  // same provisos for the aes
      provisos( Add#(data_width,0,64),
                Add#(a__, 8, addr_width),
                Add#(h__, 1, data_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=mme_clock);
  	AXI4_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Slave_Xactor();
  
  	if(!sync_required)begin // If aes is clocked by core-clock.
  		UserInterface#(addr_width, data_width, 2048) user_ifc<- mkuser_mme();
  		//UserInterface#(addr_width) user_ifc<- mkuser_aes(clocked_by mme_clock, 
      //                                                             reset_by mme_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- user_ifc.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR,rid:rd_req.arid,rlast:(rd_req.arlen==0), 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- user_ifc.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid:wr_data.wid};
      		s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
      method Bool can_take_inp = user_ifc.can_take_req;
      method Bool outp_ready = user_ifc.can_take_req;
  		//interface io= user_ifc.io;
  	end
  	else begin // if core clock and mme_clock is different.
  		UserInterface#(addr_width, data_width, 2048) user_ifc<- mkuser_mme;
  		SyncFIFOIfc#(AXI4_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,mme_clock);
  		SyncFIFOIfc#(AXI4_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,mme_clock);
  		SyncFIFOIfc#(AXI4_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,mme_clock);
  		SyncFIFOIfc#(AXI4_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  															mkSyncFIFOToCC(3,mme_clock,mme_reset);
  		SyncFIFOIfc#(AXI4_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,mme_clock,mme_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- user_ifc.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR, rid:rd_req.arid,rlast:(rd_req.arlen==0),
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- user_ifc.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid:wr_data.wid};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
      method Bool can_take_inp = user_ifc.can_take_req;
      method Bool outp_ready = user_ifc.can_take_req;
  		//interface io= user_ifc.io;
  	end
  endmodule:mkmme_axi4

endpackage

import aes :: *;
import BRAM :: *;
import bram :: *;
import DefaultValue :: *;
typedef enum {
	Start, IsReady, Ready, Idle, NotRYet
} AES_State_type deriving(Bits, Eq, FShow);

function BRAMRequest#(Bit#(128), Bit#(128)) makeRequest(Bool write, Bit#(128) addr, Bit#(128) data);
	return BRAMRequest {
		write : write,
		responseOnWrite : False,
		address : addr,
		datain : data
	};
endfunction

module mkAesTest(Empty);
	Reg#(Bit#(128)) plaintext <- mkReg(128);
	Reg#(Bit#(128)) ciphertext <- mkReg(128);
	Reg#(Bit#(128)) key <- mkReg(128);
	Reg#(Bool) _ready <- mkReg(False);
	Reg#(Bool) _done <- mkReg(False);
	Reg#(AES_State_type) rg_state <- mkReg(Idle);
	AES aes_ <- mkAES;
	Reg#(Bit#(128)) counter <- mkReg(8);

	//Ifc_bram_axi4#(`paddr, ELEN, USERSPACE, `Addr_space) main_memory <- mkbram_axi4('h00000000,"code.mem", "MainMEM");
	//BRAM_Configure cfg = defaultValue;
	//cfg.allowWriteResponseBypass = False;
	//cfg.loadFormat = tagged Hex "../verif.txt";
	//BRAM#(Bit#(128), Bit#(128)) dut0 <- mkBRAM2Server(cfg);
	//BRAM2Port#(Bit#(128), Bit#(128)) dut0 <- mkBRAM2Server(cfg);
	//cfg.loadFormat = tagged Hex "../verif_enc.txt";
	//BRAM#(Bit#(128), Bit#(128)) dut1 <- mkBRAM2Server(cfg);
	//BRAM2Port#(Bit#(128), Bit#(128)) dut0 <- mkBRAM2Server(cfg);
	UserInterface#(128,  128, 16) dut0 <- mkbram(0, "./src/verif.txt", "verifbram");
	UserInterface#(128,  128, 16) dut1 <- mkbram(0, "./src/verif_enc.txt", "verifbram_enc");

	
	//rule run(_ready == False && _done == False);
	rule idlerule(rg_state == Idle);
		counter <= 0;
		rg_state <= Start;
	endrule
	rule run(rg_state == Start);
		//if(_ready == False) aes_.encrypt(128'h3243f6a8885a308d313198a2e0370734, 128'h2b7e151628aed2a6abf7158809cf4f3c, False);
		//else if(counter == 1) aes_.encrypt(128'h3925841d02dc09fbdc118597196a0b32, 128'h2b7e151628aed2a6abf7158809cf4f3c, True);
		//_done <= True;
		//if(_ready == False) aes_.encrypt(dut0.portA.request.get(counter), 128'h5f4dcc3b5aa765d61d8327deb882cf99, False);
		//else if(counter == 50000) aes_.encrypt(dut0.portA.request.get, 128'h5f4dcc3b5aa765d61d8327deb882cf99, True);
		//rg_state <= IsReady;
		//dut0.portA.request.put(makeRequest(False, counter, ?));
		//dut1.portA.request.put(makeRequest(False, counter, ?));
		dut0.read_request(counter<<4);
		dut1.read_request(counter<<4);
		rg_state <= NotRYet;
		_ready <= False;
		//$display("Start");

		//counter <= 0;
	endrule
	rule actuallyRun(rg_state == NotRYet);
		if(_ready == False) begin
			let data <- dut0.read_response();
			//$display("NRY %h", tpl_2(data));
			//let data <- dut0.portA.response.get();
			aes_.encrypt(tpl_2(data), 128'h5f4dcc3b5aa765d61d8327deb882cf99, False);
		end
		rg_state <= IsReady;
	endrule
	//rule is_ready(_ready == False && _done == True);
	rule is_ready(rg_state == IsReady);
		_ready <= aes_.outp_ready();
		if(_ready)
			rg_state <= Ready;
	endrule
	//rule ret(_ready == True && _done == True);
	rule ret(rg_state == Ready);
		let res <- aes_.ret();	
		if(counter < 50000) begin
			//_ready <= False;
			//dut1.portA.request.put(makeRequest(False, counter, ?));
			let data_ <- dut1.read_response();
			//$display("%h, %h", res, tpl_2(data_));
			//$display("%h, %h", res, data_);
			rg_state <= Start;
			counter <= counter + 1;
			if(res != tpl_2(data_)) begin
				$display("Fail %h, %h", res, tpl_2(data_));
				$finish(0);
			end
		end
		else begin
			$display("PASS");
			$finish(0);
		end
	endrule
endmodule

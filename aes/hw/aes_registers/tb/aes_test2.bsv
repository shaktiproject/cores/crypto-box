import aes :: *;
import DefaultValue :: *;
typedef enum {
	Start, IsReady, Ready, Idle, NotRYet
} AES_State_type deriving(Bits, Eq, FShow);

(*synthesize*)
module mkAesTest(Empty);
	Reg#(Bit#(128)) plaintext <- mkReg(128);
	Reg#(Bit#(128)) ciphertext <- mkReg(128);
	Reg#(Bit#(128)) key <- mkReg(128);
	Reg#(Bool) _ready <- mkReg(False);
	Reg#(Bool) _done <- mkReg(False);
	Reg#(AES_State_type) rg_state <- mkReg(Idle);
	AES#(16) aes_ <- mkAES;
	Reg#(Bit#(128)) counter <- mkReg(8);

  rule idlerule(rg_state == Idle);
		counter <= 0;
		rg_state <= Start;
	endrule
	rule run(rg_state == Start);
		//if(_ready == False) aes_.encrypt(128'h0, 128'h0, False);
    if(_ready == False) begin
        $display("START");
        if(counter==0) aes_.encrypt(128'h5af3da146f8bd15b82e84a68f7fdec2d, 256'ha, False,Bit128);
        if(counter==1) aes_.encrypt(128'h8f02aa16b501d2726d812d6dc9b17c1c, 256'h1, False,Bit128);
        if(counter==2) aes_.encrypt(128'hf72f318c2881eb281bcc7134d8ddd557, 256'h1, True,Bit128);
        if(counter==3) aes_.encrypt(128'he3d4896c7ef2e55746193887e0f77c5b, 256'ha, True,Bit128);
        
        if(counter==4) aes_.encrypt(128'h651aad45dde54f54b549751556232561, 256'h4846, True,Bit128);
        if(counter==5) aes_.encrypt(128'h132123148846a46d6556ddd566f1c102, 256'h86, False,Bit128);
        if(counter==6) aes_.encrypt(128'hf72f318c2881eb281bcc7134d8ddd557, 256'h1544, True,Bit128);
        if(counter==7) aes_.encrypt(128'hfc09db063c8386e112e3cad4c33cef1b, 256'ha565, False,Bit128);
        
        if(counter==8) aes_.encrypt(128'h9f09319ef6f3832e9a7b543018763d01, 256'h4846, False,Bit128);
        if(counter==9) aes_.encrypt(128'hb67c2de138954328f385a85cba740d69, 256'h86, True,Bit128);
        if(counter==10) aes_.encrypt(128'hccb3aeb8d1bdc9758f3ace9ec30e4593, 256'h1544, False,Bit128);
        if(counter==11) aes_.encrypt(128'h6e8a86d729be47000d6da4e92c2bc6da, 256'ha565, True,Bit128);
        
        if(counter==12) aes_.encrypt(128'had973dd62dc1c6c21b1def0e4eadce1a, 256'h222, True,Bit128);
        if(counter==13) aes_.encrypt(128'h467b9b7552f076f2483c332c1de55beb, 256'h15665146161561, True,Bit128);
        if(counter==14) aes_.encrypt(128'he09123e039262237eb988e1f31870ea1, 256'h281658, True,Bit128);
        if(counter==15) aes_.encrypt(128'h6c847ba296d140183bf8e3867f872634, 256'h7898453, True,Bit128);
        
        if(counter==16) aes_.encrypt(128'h9878d5844a9576699ceddfb889da25bd, 256'h222, False,Bit128);
        if(counter==17) aes_.encrypt(128'he98e0a7d374db8d0d53f3823504697e0, 256'h15665146161561, False,Bit128);
        if(counter==18) aes_.encrypt(128'hb14497578e4a8cb7c9914f29f4de0ac3, 256'h281658, False,Bit128);
        if(counter==19) aes_.encrypt(128'h944247558e0e393331f7b372704dbb7c, 256'h7898453, False,Bit128);


    end
  	rg_state <= Ready;
		_ready <= False;
	endrule
		rule ret(rg_state == Ready);
		let res <- aes_.ret();	
		if(counter < 50000) begin
			rg_state <= Start;
			counter <= counter + 1;
			$display("%h", res);
      if(counter==19) begin
				$finish(0);
      end
		end
		else begin
			$finish(0);
		end
	endrule
endmodule


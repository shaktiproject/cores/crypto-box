package gen_round;

import col_shift :: *;
import sbox2 :: *;
import Vector :: *;
import invMixCols :: *;
import ConfigReg :: *;

interface GenRound#(numeric type n_sbox);
	method Action roundIn(Bit#(32) col_in0,Bit#(32) col_in1,Bit#(32) col_in2,Bit#(32) col_in3, Bool lastRound, Bool decrypt_);
	method Bit#(128) roundOut();
	method Bool roundDone();
endinterface

typedef enum {
	Idle, Ready, Sub_cols, RC_SHIFT, ADD_KEY, NR_Decrypt, INV_SUB_COLS, DEC_ADDKEY, INV_MIX_COLS
} Round_State_type deriving(Bits, Eq, FShow);

module mkGenRound#(Bit#(128) key, Vector#(n_sbox, Sbox2) sboxes)(GenRound#(n_sbox));
	let nsb = valueOf(n_sbox);
	let key0= key[127:96], key1= key[95:64], key2= key[63:32], key3= key[31:0];
	Reg#(Bit#(8)) counter <- mkReg(8); 
	//Reg#(Bool) start_sub <- mkReg(False), start_row_col <- mkReg(False), start_add <- mkReg(False);
	Reg#(Bool) decrypt <- mkReg(False), round_done <- mkConfigReg(False), new_round <- mkReg(False);

	Reg#(Bool) last_round <- mkReg(False);

	Reg#(Round_State_type) rg_state <- mkConfigReg(Idle);

	//Sbox2 sb <- mkSbox2;
	//InvSbox isb <- mkInvSbox;

	//Reg#(Bit#(8)) sbox_out <- mkReg(8);
	//Reg#(Bit#(8)) i_sbox_out <- mkReg(8);

	//Reg#(Bit#(32)) col0 <- mkReg(32), col1<- mkReg(32), col2<- mkReg(32), col3<- mkReg(32);
	//Reg#(Bit#(32)) col0mix <- mkReg(32), col1mix<- mkReg(32), col2mix<- mkReg(32), col3mix<- mkReg(32);
	//Reg#(Bit#(32)) col_0 <- mkReg(32), col_1<- mkReg(32), col_2<- mkReg(32), col_3<- mkReg(32);
	Reg#(Bit#(32)) colout0<- mkReg(32), colout1<- mkReg(32), colout2<- mkReg(32), colout3<- mkReg(32);
	
	Vector#(16, Reg#(Bit#(8))) col_ <- replicateM(mkReg(32)), sub_col <- replicateM(mkReg(32));
	Vector#(n_sbox, Reg#(Bit#(8))) sboxes_out <- replicateM(mkReg(32));
	
	Col_shift c0 <- mkColShift;
	Col_shift c1 <- mkColShift;
	Col_shift c2 <- mkColShift;
	Col_shift c3 <- mkColShift;

	//ENCRYPT !!!!!!!!!!!!!!!!!!!!!!!!!!
	rule sub (rg_state == Sub_cols && (!decrypt));
		for(Integer i = 0; i < ((16/nsb)-1); i = i +1 ) begin
			if(counter == fromInteger(i)) begin
				for(Integer j = 0; j < nsb; j = j + 1) begin
					sub_col[nsb*i + j] <= sboxes_out[j];
					let sbout <- sboxes[j].getbyte(col_[nsb*(i+1) + j],decrypt);
					sboxes_out[j] <= sbout;
				end
			end
		end
		if(counter == fromInteger((16/nsb)-1)) begin
			for(Integer j = 0; j < nsb; j = j + 1) begin
				sub_col[16 - nsb + j] <= sboxes_out[j];
			end
			rg_state <= RC_SHIFT;
		end
		counter <= counter+1;
	endrule
	
	
	rule rowColShift ((rg_state == RC_SHIFT) && (!decrypt));

		if(!last_round) begin
			colout0 <= c0.shifted({sub_col[15],sub_col[10],sub_col[ 5],sub_col[ 0]}, key0);
			colout1 <= c1.shifted({sub_col[11],sub_col[ 6],sub_col[ 1],sub_col[12]}, key1);
			colout2 <= c2.shifted({sub_col[ 7],sub_col[ 2],sub_col[13],sub_col[ 8]}, key2);
			colout3 <= c3.shifted({sub_col[ 3],sub_col[14],sub_col[ 9],sub_col[ 4]}, key3);
			//$display("%h",{key0,key1,key2,key3});
			//colout0 <= c0.shifted({col0[31:24],col1[23:16],col2[15:8],col3[7:0]}, key0);
			//colout1 <= c1.shifted({col1[31:24],col2[23:16],col3[15:8],col0[7:0]}, key1);
			//colout2 <= c2.shifted({col2[31:24],col3[23:16],col0[15:8],col1[7:0]}, key2);
			//colout3 <= c3.shifted({col3[31:24],col0[23:16],col1[15:8],col2[7:0]}, key3);
		end
		else begin
			colout0 <={sub_col[15],sub_col[10],sub_col[ 5],sub_col[ 0]} ^key0; 
			colout1 <={sub_col[11],sub_col[ 6],sub_col[ 1],sub_col[12]} ^key1; 
			colout2 <={sub_col[ 7],sub_col[ 2],sub_col[13],sub_col[ 8]} ^key2; 
			colout3 <={sub_col[ 3],sub_col[14],sub_col[ 9],sub_col[ 4]} ^key3;
			//$display("%h",{key0,key1,key2,key3});
			//colout0 <={col0[31:24],col1[23:16],col2[15:8],col3[7:0]} ^key0; 
			//colout1 <={col1[31:24],col2[23:16],col3[15:8],col0[7:0]} ^key1; 
			//colout2 <={col2[31:24],col3[23:16],col0[15:8],col1[7:0]} ^key2; 
			//colout3 <={col3[31:24],col0[23:16],col1[15:8],col2[7:0]} ^key3; 
		end
		rg_state <= Idle;
		round_done <= True;
	endrule

	//DECRYPT !!!!!!!!!!!!!!!!!!!!!!!!!!
	rule subDecrypt(rg_state == INV_SUB_COLS);
		for(Integer i = 0; i < ((16/nsb)-1); i = i +1 ) begin
			if(counter == fromInteger(i)) begin
				for(Integer j = 0; j < nsb; j = j + 1) begin
					sub_col[nsb*i + j] <= sboxes_out[j];
					let sbout <- sboxes[j].getbyte(col_[nsb*(i+1) + j],decrypt);
					sboxes_out[j] <= sbout;
				end
			end
		end
		if(counter == fromInteger((16/nsb)-1)) begin
			for(Integer j = 0; j < nsb; j = j + 1) begin
				sub_col[16 - nsb + j] <= sboxes_out[j];
			end
			rg_state <= DEC_ADDKEY;
		end
		counter <= counter+1;
	endrule


	rule addKeyDecrypt(rg_state == DEC_ADDKEY && decrypt);
		colout0 <= {sub_col[15],sub_col[ 2],sub_col[ 5],sub_col[ 8]} ^ key0; 
		colout1 <= {sub_col[11],sub_col[14],sub_col[ 1],sub_col[ 4]} ^ key1; 
		colout2 <= {sub_col[ 7],sub_col[10],sub_col[13],sub_col[ 0]} ^ key2; 
		colout3 <= {sub_col[ 3],sub_col[ 6],sub_col[ 9],sub_col[12]} ^ key3;
		
		//colout0 <= {col0[31:24],col3[23:16],col2[15:8],col1[7:0]} ^ key0; 
		//colout1 <= {col1[31:24],col0[23:16],col3[15:8],col2[7:0]} ^ key1; 
		//colout2 <= {col2[31:24],col1[23:16],col0[15:8],col3[7:0]} ^ key2; 
		//colout3 <= {col3[31:24],col2[23:16],col1[15:8],col0[7:0]} ^ key3; 
		rg_state <= Idle;
		round_done <= True;
	endrule

	rule mixColsDecrypt(rg_state == INV_MIX_COLS && decrypt);
		Vector#(16, Bit#(8)) col;
		if(!last_round) begin
			let col0mix = funcInvMixCols({col_[15],col_[14],col_[13],col_[12]});
			let col1mix = funcInvMixCols({col_[11],col_[10],col_[ 9],col_[ 8]});
			let col2mix = funcInvMixCols({col_[ 7],col_[ 6],col_[ 5],col_[ 4]});
			let col3mix = funcInvMixCols({col_[ 3],col_[ 2],col_[ 1],col_[ 0]});
			
			col = toChunks({col0mix,col1mix,col2mix,col3mix});
			
			//col0mix <= funcInvMixCols(col_0);
			//col1mix <= funcInvMixCols(col_1);
			//col2mix <= funcInvMixCols(col_2);
			//col3mix <= funcInvMixCols(col_3);
			//let sbout <- sb.getbyte(funcInvMixCols(col_0)[7:0],decrypt);
			//i_sbox_out <= sbout;
		end
		else begin
			for(Integer i = 0; i < 16; i = i + 1)
				col[i] = col_[i];
			
			//col0mix <= col_0;
			//col1mix <= col_1;
			//col2mix <= col_2;
			//col3mix <= col_3;
			//let sbout <- sb.getbyte(col_0[7:0],decrypt);
			//i_sbox_out <= sbout;
		end
		for(Integer i = 0; i < 16; i = i + 1)
			col_[i] <= col[i];
		
		for(Integer i = 0; i < nsb; i = i + 1) begin
			let sbout <- sboxes[i].getbyte(col[i],decrypt);
			sboxes_out[i] <= sbout;
		end
		rg_state <= INV_SUB_COLS;
	endrule

	method Action roundIn(Bit#(32) col_in0,Bit#(32) col_in1,Bit#(32) col_in2,Bit#(32) col_in3, Bool lastRound, Bool decrypt_) if(rg_state == Idle);
		last_round <= lastRound;
		decrypt <= decrypt_;
		Vector#(16, Bit#(8)) col = toChunks({col_in0,col_in1,col_in2,col_in3});
		if (!decrypt_) begin 
			for(Integer i = 0; i < nsb; i = i + 1) begin
				let sbout <- sboxes[i].getbyte(col[i],decrypt_);
				sboxes_out[i] <= sbout;
			end
			//let sbout <- sb.getbyte(col_in0[7:0],decrypt_);
			//sbox_out<=sbout;
			rg_state <= Sub_cols;
		end
		else begin
			rg_state <= INV_MIX_COLS;
		end
		for(Integer i = 0; i < 16; i = i + 1)
			col_[i] <= col[i];
		
		counter <= 0;
		round_done <= False;
		
		//col_0 <= col_in0;
		//col_1 <= col_in1;
		//col_2 <= col_in2;
		//col_3 <= col_in3;
	endmethod

	method Bit#(128) roundOut();
		return {colout0, colout1, colout2, colout3};
	endmethod

	method Bool roundDone();
		return round_done;
	endmethod
endmodule

endpackage

package aes_read_write_req;
  import aes :: *;
  import AXI4_Lite_Types::*;
  import AXI4_Lite_Fabric::*;
  import AXI4_Types::*;
  import AXI4_Fabric::*;
  import Semi_FIFOF::*;
  import GetPut::*;
  import FIFO::*;
  import Clocks::*;
  import BUtils::*;
  import device_common::*;
  
  interface UserInterface#(numeric type addr_width);
  	method ActionValue#(Tuple2#(Bit#(32),Bool)) read_req (Bit#(addr_width) addr, AccessSize size);
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(32) data);
	  method Bool outp_ready();
    method Bool can_take_inp();
  endinterface
  
  /*`define InputText 'b00;
  `define KeyReg 'h20;
  `define OutputReg 'h40;
  `define ConfigurationReg 'h60;
  `define StatusReg 'h61;
  */
  `include "aes_include.defines"
  typedef enum {
  	Idle, Compute, Ready
  } AES_State_type deriving(Bits, Eq, FShow);
  
  module mkuser_aes(UserInterface#(addr_width))
  provisos(Add#(a__, 8, addr_width));
  	AES aes_ <- mkAES;
  	Reg#(Bit#(128)) aes_out <- mkReg(0);
  	Reg#(Bit#(128)) rg_key <- mkReg(0);
  	Reg#(Bit#(128)) rg_input_text <- mkReg(0);
  	Reg#(Bit#(1)) rg_config <- mkReg(0);
    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_outp_ready <- mkReg(False);
  
  	Reg#(Bit#(2)) input_index <- mkReg(0);
  	Reg#(Bit#(2)) key_index <- mkReg(0);
  	Reg#(Bit#(2)) output_index <- mkReg(0);
  
  	rule rl_start(rg_start && aes_.can_take_inp);
  		aes_.encrypt(rg_input_text, rg_key, unpack(rg_config));
      $display("AES: Sending inputs: %h key: %h is_decrypt: %b",rg_input_text, rg_key, unpack(rg_config));
  	endrule

  	rule rl_getOutput(aes_.outp_ready && !rg_outp_ready);
  		let lv_aes <- aes_.ret();
      $display("AES: Getting o/p: %h", lv_aes);
      rg_outp_ready<= True;
      rg_start<= False;
      aes_out<= lv_aes;
  	endrule
  	
  	method ActionValue#(Tuple2#(Bit#(32),Bool)) read_req (Bit#(addr_width) addr, AccessSize size);
      $display($time,"\tAES: Read Req: Addr: %h", addr); 
      if(truncate(addr) == `ConfigurationReg) begin
  			return tuple2(duplicate({7'd0, rg_config}), True);
      end
  		else if(truncate(addr) == `OutputReg) begin
  			output_index<= ~output_index;
        if(output_index==0) begin
          $display("AES: Output LSB %h", aes_out[63:0]);
  				return tuple2(aes_out[31:0], True);
        end
        else if(output_index==1) begin
          $display("AES: Output LSB %h", aes_out[63:0]);
  				return tuple2(aes_out[63:32], True);
        end
        else if(output_index==2) begin
          $display("AES: Output LSB %h", aes_out[63:0]);
  				return tuple2(aes_out[95:64], True);
        end
        else begin
          $display("AES: Output MSB %h", aes_out[127:64]);
          rg_outp_ready<= False;
  				return tuple2(aes_out[127:96], True);
        end
  		end
      else if(truncate(addr) == `StatusReg) begin
        $display("AES: Status read response %b", rg_outp_ready);
        Bit#(32) read_resp= duplicate({7'd0, pack(rg_outp_ready)});
  			return tuple2(read_resp, True);
      end
      else begin
  			return tuple2(?, False);
      end
  	endmethod
  
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(32) data);
      $display($time,"\tAES: Write Req: Addr: %h Data: %h", addr, data); 
  		Bool lv_success= True;
  		if(truncate(addr) == `ConfigurationReg) begin
  			rg_config <= data[0];
  		end
  		else if(truncate(addr) == `KeyReg) begin
  			key_index<= ~key_index;
        if(key_index==0) begin
  				rg_key[31:0] <= data;
          $display("AES: Key LSB %h", data);
        end
        else if(key_index==1) begin
  				rg_key[63:32] <= data;
          $display("AES: Key LSB %h", data);
        end
        else if(key_index==2) begin
  				rg_key[95:64] <= data;
          $display("AES: Key LSB %h", data);
        end
        else begin
  				rg_key[127:96] <= data;
          $display("AES: Key MSB %h", data);
        end
  		end
  		else if(truncate(addr) == `InputText) begin
  			input_index<= ~input_index;
        if(input_index==0) begin
  				rg_input_text[31:0] <= data;
          $display("AES: Input LSB %h", data);
        end
        else if(input_index==1) begin
  				rg_input_text[63:32] <= data;
          $display("AES: Input LSB %h", data);
        end
        else if(input_index==2) begin
  				rg_input_text[95:64] <= data;
          $display("AES: Input LSB %h", data);
        end
        else begin
  				rg_input_text[127:96] <= data;
          rg_start <= True;
          $display("AES: Input MSB %h", data);
        end
  		end
  		else
  			lv_success= False;
  
  		return lv_success;
  	endmethod
  
  	method can_take_inp= (aes_.can_take_inp && !rg_outp_ready);
  	method outp_ready= rg_outp_ready;
  endmodule
  
  interface Ifc_aes_axi4lite#(numeric type addr_width, 
                              numeric type data_width, 
                              numeric type user_width);
  	interface AXI4_Lite_Slave_IFC#(addr_width, data_width, user_width) slave; 
  	method Bool can_take_inp;
  	method Bool outp_ready;
 	endinterface
  
  module mkaes_axi4lite#(Clock aes_clock, Reset aes_reset)
  															(Ifc_aes_axi4lite#(addr_width,data_width,user_width))
  // same provisos for the aes
      provisos( Add#(data_width,0,32),
                Add#(a__, 8, addr_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=aes_clock);
  	AXI4_Lite_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Lite_Slave_Xactor();
  
  	if(!sync_required)begin // If aes is clocked by core-clock.
  		UserInterface#(addr_width) aes<- mkuser_aes();
  		//UserInterface#(addr_width) aes<- mkuser_aes(clocked_by aes_clock, 
      //                                                             reset_by aes_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
      		s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  	else begin // if core clock and aes_clock is different.
  		UserInterface#(addr_width) aes<- mkuser_aes;
  		SyncFIFOIfc#(AXI4_Lite_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Lite_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  															mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  endmodule:mkaes_axi4lite
  
  interface Ifc_aes_axi4#(numeric type addr_width, 
                          numeric type data_width, 
                          numeric type user_width);
  	interface AXI4_Slave_IFC#(addr_width, data_width, user_width) slave; 
  	method Bool can_take_inp;
  	method Bool outp_ready;
 	endinterface
  
  module mkaes_axi4#(Clock aes_clock, Reset aes_reset)
  															(Ifc_aes_axi4#(addr_width,data_width,user_width))
  // same provisos for the aes
      provisos( Add#(data_width,0,32),
                Add#(a__, 8, addr_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=aes_clock);
  	AXI4_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Slave_Xactor();
  
  	if(!sync_required)begin // If aes is clocked by core-clock.
  		UserInterface#(addr_width) aes<- mkuser_aes();
  		//UserInterface#(addr_width) aes<- mkuser_aes(clocked_by aes_clock, 
      //                                                             reset_by aes_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR, rlast: True, rid: rd_req.arid,
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      	let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid: wr_req.awid};
      	s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  	else begin // if core clock and aes_clock is different.
  		UserInterface#(addr_width) aes<- mkuser_aes;
  		SyncFIFOIfc#(AXI4_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  											 	mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		SyncFIFOIfc#(AXI4_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR, rlast: True, rid: rd_req.arid,
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid: wr_req.awid};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  endmodule:mkaes_axi4
endpackage

package row_shift;

import Utils :: *;

interface row_shift;
	method Action shift(bit#(32) b, Int n);
	method ActionValue bit#(32) shifted();
endinterface

module mkShift(row_shift);
	function Action shift(bit#(32) b, Int n);
		action
			b = b<<<n;
		endaction
	endfunction

	method ActionValue bit#(32) shifted();
		return b;
	endmethod
endmodule

endpackage

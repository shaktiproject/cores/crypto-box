package quad_cascade;
import quadgen3 :: *;
import Vector ::*;
`include "details.bsv"
interface Quadcas_ifc;
method Action start2(bit[1000:0] a,int q_sel,int ts);// input, n and m -degress of the poly , mux select bit qsel and ts is max no. of cascade = 15 used as constant in prog)
method bit[`nl:0] result2();
method int count2();
endinterface: Quadcas_ifc

module mk_quadcas(Quadcas_ifc);

Reg# (bit[1000:0]) b<-mkReg(0);
Reg# (bit[1000:0]) in<-mkReg(0);
Reg# (int) ct<-mkReg(0);
Reg# (int) i2<-mkReg(1);


Reg# (int) q<-mkReg(0);
Reg# (int) t<-mkReg(0);
Reg# (int) s<-mkReg(0);
Vector#(15 ,Quad_ifc) x;                // instantiating 15 quad blocks
for(Integer i3=0;i3<15;i3=i3+1)  
	x[i3]<-mk_quad;

rule cycle(s==1 && i2<16 );             //taking output from quad block 
bit[1000:0] in = 0;
in=zeroExtend(x[i2-1].result());
ct<=ct+(x[i2-1].count())+1;
x[i2].start(in);
i2<=i2+1;
if(i2==15)
	in=zeroExtend(x[i2].result());
if(i2==15)
	s<=3;
endrule


method Action start2(a,q_sel,ts);

b<=a;
s<=1;
q<=q_sel;
x[0].start(a);  // giving the input to first quad block

endmethod

method result2()if(s==3);            // required output is muxed out using q_sel

return (x[q].result()); 
endmethod
method count2()if(s==3);            // return total clock cycles

return (ct); 
endmethod

endmodule:mk_quadcas
 
module mktestcas();
Reg#(int) state<-mkReg(0);
Quadcas_ifc mt<-mk_quadcas;

rule go(state==0);
mt.start2(zeroExtend(571'b1110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
 ),3,15); // (input , quad power-1,ts is constant) 
state<=2;
endrule

rule finish(state==2);
$display("output=%b",mt.result2()); // output bits
$display("clock cycles=%d",mt.count2()); // clock cycles
state<=4;
endrule

endmodule:mktestcas

endpackage:quad_cascade

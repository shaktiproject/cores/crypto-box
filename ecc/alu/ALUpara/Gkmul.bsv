package Gkmul;

//import StmtFSM::*; 		//for testing

//-------------------------- utility functions -----------------

function Bit#(1) mxy(Bit#(n)a, Bit#(n)b, Integer i, Integer j);
	Bit#(1) ans = (a[i] ^ a[j]) & (b[i] ^ b[j]);
	return ans;
endfunction

function Bit#(1) mx(Bit#(n)a, Bit#(n)b, Integer i);
	Bit#(1) ans = a[i] & b[i];
	return ans;
endfunction



//--------------------------- GKMUL FUNCTION ---------------------------------

function Bit#(m) gkmuln(Bit#(n) a, Bit#(n) b)
	provisos( Mul#(2, n, t1), Add#(m , 1, t1));   // 2*n-1 = m; 
	Bit#(m)c = 0;
	Integer m = valueOf(n);
	
	for(Integer i=0; i<=m-2; i=i+1)begin
		c[i] = 1'b0;
		c[2*m-2-i] = 1'b0;
		for(Integer j=0; j<=i/2; j=j+1)begin
			if(i == 2*j)begin
				c[i] = c[i] ^ mx(a,b,j);
				c[2*m-2-i] = c[2*m-2-i] ^ mx(a,b,m-1-j);
			end
			else begin
				c[i] = c[i] ^ mx(a,b,j) ^ mx(a,b,i-j) ^ mxy(a,b,j,i-j);
				c[2*m-2-i] = c[2*m-2-i] ^ mx(a,b,m-1-j) ^ mx(a,b,m-1-i+j) ^ mxy(a,b,m-1-j, m-1-i+j);
			end
		end
	end
	for(Integer j=0; j<=(m-1)/2; j=j+1)begin
		if(m-1 == 2*j)begin
			c[m-1] = c[m-1] ^ mx(a,b,j);
		end
		else begin
			c[m-1] = c[m-1] ^ mx(a,b,j) ^ mx(a,b,m-1-j) ^ mxy(a,b,j, m-1-j);
		end
	end
	return c;
endfunction
	    



/*

//------------------------------------------------ Testbench ----------------------------------------------------

(* synthesize *)
module mkTest (Empty);   				// FSM based testbench
	Reg#(Bool) complete <- mkReg(False);
	
	Stmt test =
	seq
		$display(" 15 bit gkmul test\n\n");
		$display(" %d", gkmul(15'd0, 	15'd155));
		$display(" %d", gkmul(15'd150, 	15'd56));
		$display(" %d", gkmul(15'd3500, 15'd46));
		$display(" %d", gkmul(15'd53, 	15'd291));
		$display(" %d", gkmul(15'd71, 	15'd555));
		$display(" %d", gkmul(15'd9876, 15'd2354));
		$display(" %d", gkmul(15'd2634, 15'd3));
		$display(" %d", gkmul(15'd765, 	15'd565));
		$display(" %d", gkmul(15'd9, 	15'd1234));
		$display(" %d", gkmul(15'd1546, 15'd023));

		$display("\n===================================\n 14 bit gkmul test\n\n");
		$display(" %d", gkmul(14'd0, 	14'd155));
		$display(" %d", gkmul(14'd150, 	14'd56));
		$display(" %d", gkmul(14'd3500, 14'd46));
		$display(" %d", gkmul(14'd53, 	14'd291));
		$display(" %d", gkmul(14'd71, 	14'd555));
		$display(" %d", gkmul(14'd9876, 14'd2354));
		$display(" %d", gkmul(14'd2634, 14'd3));
		$display(" %d", gkmul(14'd765, 	14'd565));
		$display(" %d", gkmul(14'd9, 	14'd1234));
		$display(" %d", gkmul(14'd1546, 14'd023));
		$finish;
	endseq;
	
	FSM testFSM <- mkFSM (test);

	rule startit ;
		testFSM.start();
	endrule
	
endmodule

*/

endpackage : Gkmul

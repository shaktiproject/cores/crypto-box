
package ControlUnit3_233;

import Vector:: *;
import ALU_233:: *;

//138ca305e15db049b171132311c5075ac56533a1cccc65682bb7754dc3e
//1706d5ee3f55ee3d8e8ea94301604603df443d224b4675747cedfcfdc93



interface ControlUnit_IFC;
	method Action kVal(Bit#(32) k);
	method Bit#(466) resultOut();
	method Bit#(1) resultStat();

endinterface


typedef enum{Start,Init,Key_algo,Double,Ad,Con,Finish} Cstate deriving (Eq,Bits);

/*typedef struct {Bit#(3) muxA; 
	Bit#(3) muxB; 
	Bit#(2) muxC; 
	Bit#(2) muxD; 
	Bit#(4) quadSel;
	 } AluControlSig;
*/
module mkControlUnit (ControlUnit_IFC);

	int ksize =0;

	ALU_ifc alu <- mk_ALU();
	Vector#(2,Reg#(Bit#(233))) bankA <- replicateM( mkReg(0) );
	Vector#(4,Reg#(Bit#(233))) bankB <- replicateM( mkReg(0) );
	Vector#(2,Reg#(Bit#(233))) bankC <- replicateM( mkReg(0) );
/*
	Vector#(2, Array#(Reg#(Bit#(233)))) bankA <- replicateM(mkCReg(2,0));
	Vector#(4, Array#(Reg#(Bit#(233)))) bankB <- replicateM(mkCReg(2,0));
	Vector#(2, Array#(Reg#(Bit#(233)))) bankC <- replicateM(mkCReg(2,0));
*/
	Reg #(Bit#(32)) k <- mkReg (0);
//	Reg #(int) i <- mkReg (ksize);
	Reg #(Cstate) state <- mkReg (Start);
	Reg #(int) statecounter <- mkReg (0);
	Reg #(Bit#(1)) sigR_W <- mkReg (0);	
	Reg #(Bit#(32)) l2r_counter <- mkReg (0);
	Reg#(Bit#(1)) rg_l2r_start  <- mkReg (0);
	
	
//	(* execution_order = "initialization, double_registerStore, double_Output, addition_regStore, addition_output" *)
	
	rule initialization (state == Init);
		case(statecounter)
			0: begin 
				//bankA[0] <= 233'h0fac9dfcbac8313bb2139f1bb755fef65bc391f8b36f8f8eb7371fd558b;
				//bankB[0] <= 233'h1006a08a41903350678e58528bebf8a0beff867a7ca36716f7e01f81052;
				
				bankA[0] <= 233'h138ca305e15db049b171132311c5075ac56533a1cccc65682bb7754dc3e;
			        bankB[0] <= 233'h1706d5ee3f55ee3d8e8ea94301604603df443d224b4675747cedfcfdc93;
				
				bankC[0] <= 1;
		end
			1: begin
			        //bankA[1] <= 233'h0fac9dfcbac8313bb2139f1bb755fef65bc391f8b36f8f8eb7371fd558b;
                                //bankB[1] <= 233'h1006a08a41903350678e58528bebf8a0beff867a7ca36716f7e01f81052;
			      
			         bankA[1] <= 233'h138ca305e15db049b171132311c5075ac56533a1cccc65682bb7754dc3e;
                                 bankB[1] <= 233'h1706d5ee3f55ee3d8e8ea94301604603df443d224b4675747cedfcfdc93;

				end
			2: begin
				bankB[3] <= 233'h066647ede6c332c7f8c0923bb58213b333b20e9ce4281fe115f7d8f90ad;
				end
		endcase

		$write("rule=> initialization	");
		$display("sub_StateCounter=> %d	",statecounter);
		$display("regvalues=> %h	",bankA[0],"%h \n",bankA[1],"%h \n",bankB[0],"%h \n",bankB[1],"%h \n",bankB[2],"%h \n",bankB[3],"%h \n",bankC[0],"%h \n",bankC[1]);

		if(statecounter == 2)	begin
			statecounter <= 0;
			$display("---------------------------------------------init_done--------------------------------------------------------");
			state <= Key_algo;
			end
		else 
			statecounter <= statecounter + 1;
	endrule
	
	
	rule l2r_algo ( state == Key_algo && alu.status == 1);
                          
           if(k[31] == 0 && rg_l2r_start == 0) begin	
                   k <= k<<1 ; 
                   l2r_counter <= l2r_counter + 1 ; end       
	    else begin rg_l2r_start <= 1; end 
	 
	    if(rg_l2r_start == 1 && l2r_counter != 31) begin
	     state <= Double ;
	     k <= k<<1 ; 
	     l2r_counter <= l2r_counter + 1 ;  end
	    else if (rg_l2r_start == 1 && l2r_counter == 31 )begin
	        state <= Con; end 
	      else begin state <=  Key_algo; end
	       
	       $display("key %b	\n",k);
	       $display("l2r_counter %d	\n",l2r_counter);
	       $display("rg_l2r_start %d\n",rg_l2r_start);
	       $display("state %d\n",state);      
	
	endrule 
	
	rule double_Output (state == Double && sigR_W == 0 && alu.status == 1);
		AluControlSig cons;	
		case(statecounter)
			0: begin
				cons.muxA =3'b001;	cons.muxB =3'b001;
				cons.muxC =2'b00;		cons.muxD =2'b10;
				cons.quadSel =4'b0000;
				alu.start(bankA[0], bankC[0], 0, 0, 0, cons);	
		$display("sub_StateCounter(0)=> %d	",statecounter);
 			end
			1: begin
				cons.muxA =3'b010;	cons.muxB =3'b000;
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankB[3], bankB[2], 0, 0, cons);
				$display("sub_StateCounter(1)=> %d	",statecounter);
				end

			2: begin
				cons.muxA =3'b100;	cons.muxB =3'b100;
				cons.muxC =2'b00;		cons.muxD =2'b11;
				cons.quadSel =4'b0000;
				alu.start(bankA[0], bankB[2], bankB[0], bankC[0], 0, cons);
				end
			3: begin
				cons.muxA =3'b000;	cons.muxB =3'b000;
				cons.muxC =2'b11;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(bankB[2], bankC[0], 0, bankC[1], 0, cons);
				end
		endcase
		$write("rule=> double	output	");
		$write("State=> %d	",state);
		$display("sub_StateCounter=> %d	",statecounter);
		$display("regvalues=> %h	",bankA[0],"%h\n",bankA[1],"%h\n",bankB[0],"%h\n",bankB[1],"%h\n",bankB[2],"%h\n",bankB[3],"%h\n",bankC[0],"%h\n",bankC[1]);
		sigR_W <= 1;
	endrule

	rule double_registerStore (state == Double  && sigR_W == 1 && alu.status == 1 );
		let c0 = alu.c0;
		let c1 = alu.c1;
		let q = alu.qout;

/*		bit[1000:0] c0 =  zeroExtend(c0233);
		bit[1000:0] c1 =  zeroExtend(c1233);
		bit[1000:0] q =  zeroExtend(q233);
*/		
		case(statecounter)
				0: begin
					bankC[0] <= c0;
					bankB[2] <= c1;
				end
				1: begin
					bankB[2] <= c0;
					end
				2: begin
					bankC[1] <= c0;
					bankA[0] <= c1;
					end
				3: begin
					bankB[0] <= c0;
					end
			endcase
		
		$write("rule=> double reg store	");
		$write("State=> %d	",state);
		$display("StateCounter=> %d	",statecounter);
		$display("regvalues=> %h	",bankA[0],"%h \n",bankA[1],"%h\n",bankB[0],"%h\n",bankB[1],"%h\n",bankB[2],"%h\n",bankB[3],"%h\n",bankC[0],"%h\n",bankC[1]);

		$display("	C0=> %h	",c0);
		$display("	C1=> %h	",c1);
		
		
		if(statecounter >= 3 && k[31] == 1 ) begin 
	                       statecounter <= 0; 
	                       state <= Ad; end  
	            else if(statecounter >= 3 )  begin   
	                       statecounter <= 0; 
	                       state <= Key_algo; end                                               	
	            else statecounter <= statecounter + 1;           
			sigR_W <= 0;		
	endrule

	rule addition_output (state == Ad && sigR_W == 0 && alu.status == 1);
		AluControlSig cons;

		case(statecounter)
			0: begin
				cons.muxA =3'b000;	cons.muxB =3'b001;
				cons.muxC =2'b01;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(bankB[1], bankC[0], bankB[0], 0, 0, cons);
			end
			1: begin
				cons.muxA =3'b010;	cons.muxB =3'b000;
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start( bankA[0], bankC[0], bankA[1], 0, 0, cons);
				end
			2: begin
				cons.muxA =3'b000;	cons.muxB =3'b101;
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start( bankA[0], 0, 0, bankC[0], 0, cons);
				end
			3: begin
				cons.muxA =3'b001;	cons.muxB =3'b010;
				cons.muxC =2'b00;		cons.muxD =2'b00; 
				cons.quadSel =4'b0000;
				alu.start(bankA[0], bankC[0], bankB[2], 0, 0, cons);
				end
			4: begin
				cons.muxA =3'b010;	cons.muxB =3'b000;
				cons.muxC =2'b00;		cons.muxD =2'b01;
				cons.quadSel =4'b0000;
				alu.start(bankA[0], bankB[2], bankB[0], 0, 0, cons);
				end
			5: begin
					cons.muxA =3'b010;	cons.muxB =3'b001;
				cons.muxC =2'b10;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(bankA[0], bankB[2], bankA[1], 0, 0, cons);
				end
			6: begin
					cons.muxA =3'b011;	cons.muxB =3'b001;
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(bankB[1], bankC[0], bankA[1], 0, 0, cons);
				end
			7: begin
					cons.muxA =3'b000;	cons.muxB =3'b011;
				cons.muxC =2'b01;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(bankB[2], bankC[0], bankB[0], bankC[1], 0, cons);
				end
		endcase
		
		$write("rule=> addition output	");
		$display("StateCounter=> %d	",statecounter);
		$display("regvalues=> %h	",bankA[0],"%h\n",bankA[1],"%h\n",bankB[0],"%h\n",bankB[1],"%h\n",bankB[2],"%h\n",bankB[3],"%h\n",bankC[0],"%h\n",bankC[1]);
		
		sigR_W <= 1;
		
	endrule

	rule addition_regStore (state == Ad && sigR_W == 1 && alu.status == 1); //&& alu.status == 1 );
		let c0 = alu.c0;
		let c1 = alu.c1;
		let q = alu.qout;

			case(statecounter)
				0: begin
					bankB[0] <= c0;
				end
				1: begin
					bankA[0] <= c0;
					end
				2: begin
					bankB[2] <= c0;
					end
				3: begin
					bankA[0] <= c0;
					end
				4: begin
					bankC[1] <= c0;
					bankA[0] <= c1;
					end
				5: begin
					bankC[0] <= c0;
					bankB[2] <= c1;
					end
				6: begin
					bankB[0] <= c0;
					end
				7: begin
					bankB[0] <= c0;
					end
			endcase
	

		$write("rule=> add reg store	");
		$write("State=> %d	",state);
		$display("StateCounter=> %d	",statecounter);

		$display("	C0=> %h	",c0);
		$display("	C1=> %h	",c1);
		
		
		 if(statecounter == 7)begin
		    statecounter <= 0;
			state <= Key_algo;
		     end
		  else statecounter <= statecounter + 1;	
			
			
		sigR_W <= 0;
	endrule

	rule inverse_output (state == Con && alu.status == 1 && sigR_W == 0);
		AluControlSig cons;

		case(statecounter)
			0: begin
					cons.muxA =3'b101;	cons.muxB =3'b001;   
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[0], 0, 0, 0,cons);
			end
			1: begin
					cons.muxA =3'b110;	cons.muxB =3'b000;  
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[0], 0, 0, 0,cons);
				end
			2: begin
					cons.muxA =3'b101;	cons.muxB =3'b110;  
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[0], bankB[2], 0, 0,cons);
				end
			3: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0011;
				alu.start(0, 0, 0, 0, bankB[2], cons);
				end
			4: begin
					cons.muxA =3'b010;	cons.muxB =3'b000;  
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[1], bankB[2], 0, 0,cons);
				end
			5: begin
					cons.muxA =3'b101;	cons.muxB =3'b110;  
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[0], bankB[2], 0, 0,cons);
				end
			6: begin
					cons.muxA =3'b000;	cons.muxB =3'b000;  
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0111;
				alu.start(0, 0, 0, 0, bankB[2], cons);
				end
			7: begin
					cons.muxA =3'b010;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[1], bankB[2], 0, 0,cons);
				end
			8: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b1110;
				alu.start(0, 0, 0, 0, bankB[2], cons);
				end
			9: begin
					cons.muxA =3'b010;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[1], bankB[2], 0, 0,cons);
				end
			10: begin
					cons.muxA =3'b101;	cons.muxB =3'b110; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[0], bankB[2], 0, 0,cons);
				end
			11: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b1110;
				alu.start(0, 0, 0, 0, bankB[2], cons);
				end
			12: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b1110;
				alu.start(0, 0, 0, 0, bankC[1], cons);
				end
			13: begin
					cons.muxA =3'b010;	cons.muxB =3'b111; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[1], bankB[2], 0, 0,cons);
				end
			14: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b1110;
				alu.start(0, 0, 0, 0, bankB[2], cons);
				end
			15: begin
				cons.muxA =3'b000;	cons.muxB =3'b000;
				cons.muxC =2'b00;		cons.muxD =2'b00; 
				cons.quadSel =4'b1110;
				alu.start(0, 0, 0, 0, bankC[1], cons);
				end
			16: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b1110;
				alu.start(0, 0, 0, 0, bankC[1], cons);
				end
			17: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b1110;
				alu.start(0, 0, 0, 0, bankC[1], cons);
				end
			18: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00; 
				cons.quadSel =4'b0010;
				alu.start(0, 0, 0, 0, bankC[1], cons);
				end
			19: begin
					cons.muxA =3'b010;	cons.muxB =3'b000; 

				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankC[1], bankB[2], 0, 0,cons);
				end
			20: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b10;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(0, bankB[2], 0, 0, 0,cons);
				end
			21: begin
					cons.muxA =3'b000;	cons.muxB =3'b000; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start( bankA[0], bankC[0], 0, 0, 0,cons);
				end
			22: begin
					cons.muxA =3'b000;	cons.muxB =3'b001; 
				cons.muxC =2'b00;		cons.muxD =2'b00;
				cons.quadSel =4'b0000;
				alu.start(bankB[0], bankC[0], 0, 0, 0,cons);
				end
		endcase
		
		$write("rule=> inverse output		");
		$write("State=> %d	",state);
		$display("StateCounter=> %d	",statecounter);
		
		sigR_W <= 1;
	endrule


	rule inverse_regStore (state == Con && alu.status == 1 && sigR_W == 1);
		let c0 = alu.c0;
		let c1 = alu.c1;
		let q = alu.qout;

/*		bit[1000:0] c0 =  zeroExtend(c0233);
		bit[1000:0] c1 =  zeroExtend(c1233);
		bit[1000:0] q =  zeroExtend(q233);
*/
		case(statecounter)
			0: begin
				bankC[0] <= c0;
			end
			1: begin
				bankB[2] <= c0;
				end
			2: begin
				bankB[2] <= c0;
				end
			3: begin
				bankC[1] <= q;
				end
			4: begin
				bankB[2] <= c0;
				end
			5: begin
				bankB[2] <= c0;
				end
			6: begin
				bankC[1] <= q;
				end
			7: begin
				bankB[2] <= c0;
				end
			8: begin
				bankC[1] <= q;
				end
			9: begin
				bankB[2] <= c0;
				end
			10: begin
				bankB[2] <= c0;
				end
			11: begin
				bankC[1] <= q;
				end
			12: begin
				bankC[1] <= q;
				end
			13: begin
				bankB[2] <= c0;
				end
			14: begin
				bankC[1] <= q;
				end
			15: begin
				bankC[1] <= q;
				end
			16: begin
				bankC[1] <= q;
				end
			17: begin
				bankC[1] <= q;
				end
			18: begin
				bankC[1] <= q;
				end
			19: begin
				bankB[2] <= c0;
				end
			20: begin
				bankC[0] <= c0;
				end
			21: begin
				bankA[0] <= c0;
				end
			22: begin
				bankB[0] <= c0;
				end
		endcase
		
		if(statecounter == 22)	begin
			statecounter <= 0;
			state <= Finish;
		end
		else
				statecounter <= statecounter + 1;

		$write("rule=> inverse reg store	");
		$write("State=> %d	",state);
		$display("StateCounter=> %d	",statecounter);

		$display("	C0=> %h	",c0);
		$display("	C1=> %h	",c1);
	
		sigR_W <= 0;
	endrule



	rule finish (state == Finish);
		$display("Computation is finished ");
	endrule

	method Action kVal(Bit#(32) valueK) if (state == Start);
		k <= valueK;
		state <= Init;
	endmethod

	method Bit#(1) resultStat();
	
		if (state == Finish)
				return 1;
		else return  0;
	endmethod 

	method Bit#(466) resultOut() if (state == Finish);
		let x1 = bankA[0];
		let y1 = bankB[0];
//		let z1 = bankC[0];
				Bit#(466) result = {x1,y1};
				return result;

/*		if (state == finish)
			begin
				Bit#(699) result = {x1,y1,z1};
				return result;
			end
*/		
	endmethod 
endmodule

endpackage

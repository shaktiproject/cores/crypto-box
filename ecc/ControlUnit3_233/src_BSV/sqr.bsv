package sqr;
import Vector::*;

interface Sqr_ifc;
method Action start(Bit#(233) a); // input for quad block


method Bit#(233) result();

endinterface: Sqr_ifc

module mk_sqr(Sqr_ifc);

Reg# (Bit#(233)) b<-mkReg(0);

Reg# (Bit#(467)) x<-mkReg(0);

Reg# (int) s<-mkReg(0);
Reg# (int) ct<-mkReg(0);
Reg#(int) i2<-mkReg(0);

rule add_Zero(s==1);
x[2*i2]<=b[i2];
i2<=i2+1;
if(i2==233)
	s<=2;
endrule
       





rule cyclce(s==2);


Bit#(233) x1 =(x[466:(((232)*2)+2-74)])^(x[(466):(((232)*2)+2)])^(x[(466):(((232)*2)+2)]); // modulo by polynomial
x1 = x1^(x1<<74)^(x1<<0)^(x1<<0)^(x[(232):0])^(x[((232)*2):233])^(x[((232)*2):233]<<74)^(x[((232)*2):233]<<0)^(x[((232)*2):233]<<0);   // square is performed

b<=x1;

s<=4;
ct<=ct+1;
endrule


method Action start(a);


b<=a;
s<=1;
i2<=0;
endmethod


method result()if(s==4);
return (b); // output of the quad block
endmethod

endmodule:mk_sqr 

module mktest(); //testbench
Reg#(int) state<-mkReg(0);
Sqr_ifc m<-mk_sqr;

rule go(state==0);
m.start(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
 );         // change the input according to the field
state<=2;
endrule

rule finish(state==2);
$display("Quad=%b",m.result()); // output is displayed in  bits.

state<=4;
endrule
rule go2(state==4);
m.start( 233'b01010100000000000000000000000000000000000000000000000000000000000000000000000000000000101010000000000000000000000000000000000000000000000000000000000000000000001010100000000000000000000000000000000000000000000000000000000000000000000
);         // change the input according to the field
state<=7;
endrule

rule finish2(state==7);
$display("Quad=%b",m.result()); // output is displayed in  bits.

state<=8;
endrule

endmodule:mktest

endpackage:sqr

import math, sys
print("=======================================================")
print("|                HKMUL BSV GENERATOR                  |")
print("=======================================================")
print("by: Saurabh Singh\n")
hkdefs = []
gkdefs = []
Code = []

def SKwriter(n):
    code=["\n//----------------- HKMUL"+str(n)+"-------------------"]
    code+=[ "function Bit#("+str(2*n-1)+") hkmul"+str(n)+"(Bit#("+str(n)+") a, Bit#("+str(n)+") b);" ]
    l = math.ceil(n/2)
    code+=[ "\tInteger n = "+str(n)+";  Integer l = "+str(l)+";"]

    if n%2==0:
        code+=["\tBit#(l)	aDash = a[n-1:l] ^ a[l-1:0];"]
        code+=["\tBit#(l)	bDash = b[n-1:l] ^ b[l-1:0];"]
    else:
        code+=[ "\tBit#(l) aDash = a[n-1:l] ^ a[l-2:0]; aDash[l-1] = a[l-1];" ]

        code+=[ "\tBit#(l)	bDash = b[n-1:l] ^ b[l-2:0]; bDash[l-1] = b[l-1];" ]

    if (l < 29):
        code+=["\tBit#("+str(2*l-1)+") cp1 = gkmul"+str(l)+"(a[l-1:0], b[l-1:0]);"]
        code+=["\tBit#("+str(2*l-1)+") cp2 = gkmul"+str(l)+"(aDash, bDash);" ]
    else:
        code+=["\tBit#("+str(2*l-1)+") cp1 = hkmul"+str(l)+"(a[l-1:0], b[l-1:0]);"]
        code+=["\tBit#("+str(2*l-1)+") cp2 = hkmul"+str(l)+"(aDash, bDash);" ]
        
    if (n-l<29) :
        code+=["\tBit#("+str(2*(n-l)-1)+") cp3 = gkmul"+str(n-l)+"(a[n-1:l], b[n-1:l]);" ]
    else:
        code+=["\tBit#("+str(2*(n-l)-1)+") cp3 = hkmul"+str(n-l)+"(a[n-1:l], b[n-1:l]);" ]

    #here is the change
    
    code+=["\tBit#("+str(2*n-1)+") cP1 = zeroExtend(cp1);" ]
    code+=["\tBit#("+str(2*n-1)+") cP2 = zeroExtend(cp2);" ]
    code+=["\tBit#("+str(2*n-1)+") cP3 = zeroExtend(cp3);" ]
    code+=["\tBit#("+str(2*n-1)+") ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1;" ]
    code+=["\treturn ans;" ]
    code+=["endfunction" ]
	
    return code



def GKwriter(n):
    code=[ "\n//---------------- GKMUL"+str(n)+"---------------" ]
    code+=[ "function Bit#("+str(2*n-1)+") gkmul"+str(n)+"(Bit#("+str(n)+") a, Bit#("+str(n)+") b);" ]
    
    code+=["\tBit#("+str(n)+") n;"]
    code+=["\tfor(Integer i=0; i<"+str(n)+"; i=i+1)begin"]
    code+=["\t\tn[i] = a[i] & b[i];"]
    code+=["\tend"]

    code+=["\tBit#("+str(n)+") m["+str(n)+"];"]
    code+=["\tfor(Integer i=0; i<"+str(n)+"; i=i+1)begin"]
    code+=["\t\tfor(Integer j=i+1; j<"+str(n)+"; j=j+1)begin"]
    code+=["\t\t\tm[i][j] = (a[i] ^ a[j]) & (b[i] ^ b[j]);"]
    code+=["\t\tend\n\tend"]
    code+=["\tBit#("+str(2*n-1)+") d;"]
    
    for i in range(0,n):
        tmp="\td["+str(i)+"] = "
        for j in range(0,i):
            if j >= i-j:
                continue

            tmp+="m["+str(j)+"]["+str(i-j)+"] ^ "
		
        for j in range(0,i):
            tmp+="n["+str(j)+"] ^ "
		
        tmp+="n["+str(i)+"];"
        code+=[tmp]

    for i in range(n,2*n-1):
        tmp="\td["+str(i)+"] = "
        for j in range(i-n+1, i):
            if(j >= i-j):
                break
            tmp+="m["+str(j)+"]["+str(i-j)+"] ^ "
        
        for j in range(i-n+1, n-1):
            tmp+="n["+str(j)+"] ^ "
		
        tmp+="n["+str(n-1)+"];"
        code+=[tmp]
    
    code+=["\treturn d;"]
    code+=["endfunction"]

    return code
    


def main(b):
    global hkdefs,gkdefs
    if b<29:
        if b not in gkdefs:
            gkdefs+=[b]
        return

    hkdefs+=[b]

    if math.ceil(b/2) not in hkdefs:
        main(math.ceil(b/2))
    
    if math.floor(b/2) not in hkdefs:
        main(math.floor(b/2))


def write(fname, txt):  #txt is a list
    try:
        f = open(fname, "w")
        for line in txt:
            f.write(line+"\n")
        f.close()
    except IOError:
        sys.exit("Error: Unable to create file")

try:
    b = int(input("bits? : "))
except ValueError:
    sys.exit("Error: Unexpected input;\nThis program is not a text editor!")
if b==0:
    sys.exit("Error: Can't generate a zero bit multiplier;\n")

main(b)
hkdefs.sort(reverse=True)
gkdefs.sort(reverse=True)

print("generating code...\n")
Code+=["package hkmul;"]
for i in hkdefs:
    Code+=SKwriter(i)
    print("SKmul-"+str(i))
print("--------")
for i in gkdefs:
    Code+=GKwriter(i)
    print("GKmul-"+str(i))

Code+=["endpackage"]


Code=["/* This is an auto generated Bluespec SystemVerilog code for "+str(b)+"""-bit finite field hybrid karatsuba multiplier
* Code By			 : Saurabh Singh
* Design Proposed by : Chester Rebeiro
*/"""]+Code

write("hkmul.bsv", Code)
print(hkdefs)
print(gkdefs)
print("\ndone!")

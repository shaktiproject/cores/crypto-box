package testbench;

import Vector              ::*;
import utils               ::*;
import GetPut              ::*;
`ifdef sequential
  import sequential_sha_engine ::*;
`else
  import pipeline_sha_engine ::*;
`endif
import LFSR                ::*;
import FIFOF               ::*;
import "BDPI" function Bit#(256) sha256_final (Bit#(512) data);

module mk_test(Empty);
  Reg#(Bit#(64)) count <-mkReg(0);
  Reg #(Bool)initial_state<- mkReg(True);
  
  Vector #(16,LFSR#(Bit#(32))) lfsrs      <- replicateM(mkLFSR_32);

  FIFOF #(Bit#(256)) c_result <- mkSizedFIFOF(66);
  FIFOF #(Bit#(512)) input_store <- mkSizedFIFOF(66);

  `ifdef sequential
    Ifc_sha_engine engine <- mksequential_sha_engine;
  `else
    Ifc_sha_engine engine <- mkpipeline_sha_engine;
  `endif
  
  rule initialisation(initial_state);
    let x = 'hdeadbeef;
    Integer k=12;
    for(Integer i=0;i<16;i=i+1)
      lfsrs[i].seed(x<<i+k);
    initial_state <= False;
    `ifdef verbose `ifdef sequential $display("SEQUENTIAL"); `else $display("Pipeline"); `endif 
    `endif
  endrule
  rule input_e(!initial_state);
    if(engine.ready)begin
      Bit#(32) inputs[16];
      for(Integer i=0;i<16;i=i+1)begin
        inputs[i] = lfsrs[i].value();
        lfsrs[i].next();
      end
      Bit#(256) ph='h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
      Bit#(512) message={inputs[0],inputs[1],inputs[2],inputs[3],inputs[4],inputs[5],inputs[6],
                          inputs[7],inputs[8],inputs[9],inputs[10],inputs[11],inputs[12],
                          inputs[13],inputs[14],inputs[15]};
      Bit#(256) tmp1={inputs[7],inputs[6],inputs[5],inputs[4],inputs[3],inputs[2],inputs[1],inputs[0]};
      Bit#(256) tmp2={inputs[15],inputs[14],inputs[13],inputs[12],inputs[11],inputs[10],inputs[9],inputs[8]};
      c_result.enq(sha256_final({tmp2,tmp1}));
      input_store.enq(message);
      engine.input_engine(ph,message);
      `ifdef verbose $display("TB:%8h Ph:%64h M:%128h",cur_cycle,ph,message); `endif
    end
  endrule
  rule output_e(!initial_state);
    let x <- engine.output_engine.get;
    let y = c_result.first;c_result.deq;
    if(x==y)begin
      input_store.deq;
    end
    else begin
      $display("ERROR!!!!!!!!");
      $display("TB:INPUT:%128h",input_store.first);
      $display("TB:ENGINE:%64h",x);
      $display("TB:C:%64h",y);
      $finish;
    end
    if(count=='hFFFFFFFFFFFFFFFF)
      $finish;
    if(count%10000000==0)
      $display("%16h",count);
    count<=count+1;
  endrule

endmodule:mk_test
endpackage:testbench

package sha_accelerator_v2;

`include "defined_parameters.bsv"

/* ======== Package imports ======= */
import utils         ::*;
import FIFOF         ::*;
import ConfigReg     ::*;
import Connectable   ::*;
import GetPut        ::*;
import DefaultValue  ::*;
/*================================== */

/*========= Project imports ======== */
import AXI4_Types		        :: *;
import AXI4_Fabric	        :: *;
import pipeline_sha_engine  :: *;
import Semi_FIFOF           :: *;
/*================================== */


/*
0-Start Addr
1-No of read bursts=no of entries/(512/passlengthinbits)*256
*/
typedef Bit#(`PADDR) Req_Addr;
typedef Bit#(`Reg_width) Req_Data;
typedef Bit#(`USERSPACE) Req_Info;

interface Ifc_accelerator;
  method Action start;
  interface AXI4_Master_IFC#(`PSHAADDR, 512, `USERSPACE) data_channel;
  interface AXI4_Slave_IFC#(`PADDR, `Reg_width, `USERSPACE) config_channel;
endinterface

module mkAccelerator(Ifc_accelerator);
  AXI4_Slave_Xactor_IFC #(`PADDR,`Reg_width,`USERSPACE) s_xactor <- mkAXI4_Slave_Xactor;
  AXI4_Master_Xactor_IFC #(`PSHAADDR,512,`USERSPACE) m_xactor <- mkAXI4_Master_Xactor;

  Reg#(Bit#(`Reg_width))   config_regs[2];
  Reg#(Bit#(256))          hash<-mkReg(0);
  Reg#(Bit#(1))            status <- mkReg(0);
  Reg#(Bit#(`Reg_width))   read_transact_counter <- mkReg(0);
  Reg#(Bit#(`Reg_width))   write_transact_counter <- mkReg(0);
  Reg#(Bit#(8))            index <- mkReg(0);
  Reg#(Bit#(`Reg_width))   addr_reg<- mkRegU;
  Reg#(Bit#(0))            nullReg <- mkReg( ? ) ;
  Reg#(Bit#(`Reg_width))   readAddress<-mkReg(0);
  Reg#(Bit#(`Reg_width))   addr_pass<-mkReg(0);

  Reg#(Bit#(TLog#(`Number_of_groups))) input_status<-mkReg(0);
  Reg#(Bit#(TLog#(Mul#(64,`Number_of_hash_cycles)))) counter[`Number_of_groups];
  Reg#(Bit#(1)) counter_stat[`Number_of_groups]<-mkReg(0);

  FIFOF #(Bit#(`Reg_width)) addr[`Num_of_engines];
  
  Ifc_sha_engine engine[`Num_of_engines];

  for(Integer i=0;i<2;i=i+1)
    config_regs[i]<-mkRegU;
  for(Integer i=0;i<`Num_of_engines;i=i+1)begin
    engine[i] <- mkpipeline_sha_engine;
    pass[i]   <- mkSizedFIFOF(65);
  end
  for(Integer i=0;i<`Number_of_groups;i=i+1)begin
    counter[i]<-mkReg(0);
    counter_stat<-mkReg(0);
  end


  `ifdef SYS64
    function ActionValue#(Bool) writeReg(Req_Addr addr,Req_Data data)
    =actionvalue
      Bit#(8) taddr = truncate(addr);
      case(taddr)
        ((`Reg_width/8)*0)  : begin
                              status<=data;
                              return True;
                              end
        ((`Reg_width/8)*1)  : begin
                              config_regs[0]<=data;
                              return True;
                              end
        ((`Reg_width/8)*2)  : begin
                              config_regs[1]<=data;
                              return True;
                              end
        ((`Reg_width/8)*3)  : begin
                              hash[63:0]<=data;
                              return True;
                              end
        ((`Reg_width/8)*4)  : begin
                              hash[127:64]<=data;
                              return True;
                              end
        ((`Reg_width/8)*5)  : begin
                              hash[191:128]<=data;
                              return True;
                              end
        ((`Reg_width/8)*6)  : begin
                              hash[255:192]<=data;
                              return True;
                              end
        default             : return False;
      endcase
    endactionvalue;
  `endif

  `ifdef SYS32
    function ActionValue#(Bool) writeReg(Req_Addr addr,Req_Data data)
    =actionvalue
      Bit#(8) taddr = truncate(addr);
      case(taddr)
        ((`Reg_width/8)*0)  : begin
                              status<=data;
                              return True;
                              end
        ((`Reg_width/8)*1)  : begin
                              config_regs[0]<=data;
                              return True;
                              end
        ((`Reg_width/8)*2)  : begin
                              config_regs[1]<=data;
                              return True;
                              end
        ((`Reg_width/8)*3)  : begin
                              hash[31:0]<=data;
                              return True;
                              end
        ((`Reg_width/8)*4)  : begin
                              hash[63:32]<=data;
                              return True;
                              end
        ((`Reg_width/8)*5)  : begin
                              hash[95:64]<=data;
                              return True;
                              end
        ((`Reg_width/8)*6)  : begin
                              hash[127:96]<=data
                              return True;
                              end
        ((`Reg_width/8)*7)  : begin
                              hash[159:128]<=data
                              return True;
                              end
        ((`Reg_width/8)*8) : begin
                              hash[191:160]<=data
                              return True;
                              end
        ((`Reg_width/8)*9) : begin
                              hash[223:192]<=data
                              return True;
                              end 
        ((`Reg_width/8)*10) : begin
                              hash[255:224]<=data
                              return True;
                              end                     
        default             : return False;
      endcase
    endactionvalue;
  `endif


  rule writeConfig(status=='h0);
    let write_addr <- pop_o(s_xactor.o_wr_addr);
    let write_data <- pop_o(s_xactor.o_wr_data);
    Req_Data lv_data= 0;
    Req_Addr lv_addr= 0;
    AXI4_Resp lv_bresp= AXI4_OKAY;
    Bool lv_send_response= True;

    if(write_data.wstrb=='hFF) begin
      lv_data= write_data.wdata;
      lv_addr= write_addr.awaddr;
    end
    else begin
      lv_bresp= AXI4_SLVERR;
      $display($time,"\tDMA: KAT GAYA");
    end
  
    let lv1<-writeReg(lv_addr,lv_data);
    if(!lv1) begin	//if no register mapping exists for the given address
      lv_bresp= AXI4_SLVERR;
      $display($time,"\tDMA: Wapas KAT GAYA");
    end
    else begin
      lv_bresp = AXI4_OKAY;
      let resp = AXI4_Wr_Resp { bresp: lv_bresp, buser: 0, bid: write_addr.awid };
      s_xactor.i_wr_resp.enq(resp);
    end

    if(lv_addr=='h0)begin
      readAddress<=config_regs[0];
      addr_reg<=config_regs[0];
      engine[1].reset;
      engine[0].reset;
      input_status<=0;
      for(Integer i=0;i<`Number_of_groups;i=i+1)begin
        counter[i]<=0;
        counter_stat<=0;
      end
    end
  endrule

  rule createReadTransactions(status=='h1);
    if(read_transact_counter<config_regs[1])begin
    let read_request = AXI4_Rd_Addr {araddr: readAddress, 
    arid: `Sha_accelerator_id, arlen: 'hFF,
    arsize: 'd6, arburst: 'd1, //arburst: 00-FIXED 01-INCR 10-WRAP
    aruser: 0 };
    m_xactor.i_rd_addr.enq(read_request);
    read_transact_counter<=read_transact_counter+1;
    readAddress<=readAddress+'d512;
    end
  endrule



  /*
  rule serviceReadResponses((status=='h1)&&
        (m_xactor.o_rd_data.first.rid==`Sha_accelerator_id));
    if(engine[0].ready&&engine[1].ready)begin
      Bit#(512) input1,input2;
      if(config_regs[3]==8)begin
      input2={m_xactor.o_rd_data.first.rdata[127:64]
                          ,8'h80,'d0,config_regs[3]<<'d3};
      input1={m_xactor.o_rd_data.first.rdata[63:0]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      end
      else if(config_regs[3]==16)begin
      input2={m_xactor.o_rd_data.first.rdata[255:128]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      input1={m_xactor.o_rd_data.first.rdata[127:0]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      end
      else begin
      input2={m_xactor.o_rd_data.first.rdata[511:256]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      input1={m_xactor.o_rd_data.first.rdata[255:0]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      end
      Bit#(256) ph='h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
      engine[0].input_engine(ph,input1);
      engine[1].input_engine(ph,input2);
      m_xactor.o_rd_data.deq;
    end
  endrule
*/
  rule processOutput(status=='h1);
    Bit#(1) fstatus;
    Bit#(1) check[`Num_of_engines];
    Bit#(`Pass_length) cracked;
    cracked=password;
    fstatus=status;
    for(Integer i=0;i<`Num_of_engines;i=i+1)begin
      let data <- engine[i].output_engine.get;
      if(data==hash)begin
        check[i]='h1;
        cracked=pass[i].first;
      end
      else
        check[i]='h0;
    end
    for(Integer i=0;i<`Num_of_engines;i=i+1)begin
      fstatus = fstatus & ~check[i];
      pass[i].deq;
    end
    if(fstatus==0)
      password<=cracked;
    status<=fstatus;
  endrule

  interface config_channel=s_xactor.axi_side;
  interface data_channel=m_xactor.axi_side;
endmodule:mkAccelerator
endpackage:sha_accelerator_v2
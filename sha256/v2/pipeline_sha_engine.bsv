package pipeline_sha_engine;

`include "defined_parameters.bsv"

import utils     ::*;
import BRAMFIFO  ::*;
import GetPut    ::*;
import DReg      ::*;
import FIFOF     ::*;


interface Ifc_sha_engine;
  method Action reset;
  method Bool ready;
  method Action input_engine(Bit#(256) pre_hash,Bit#(512) input_val);
  interface Get#(Bit#(256)) output_engine;
endinterface

function Bit#(32) rotr (Bit#(32) x,Integer n);
  Bit#(64) y={x,'d0} >> n;
  return y[63:32]|y[31:0];
endfunction:rotr
  
function Bit#(32) s0 (Bit#(32) x);
  return rotr(x,2)^rotr(x,13)^rotr(x,22);
endfunction:s0
  
function Bit#(32) s1 (Bit#(32) x);
  return rotr(x,6)^rotr(x,11)^rotr(x,25);
endfunction:s1
  
function Bit#(32) c0 (Bit#(32) x);
  return rotr(x,7)^rotr(x,18)^(x>>3);
endfunction:c0
  
function Bit#(32) c1 (Bit#(32) x);
  return rotr(x,17)^rotr(x,19)^(x>>10);
endfunction:c1
  
function Bit#(32) ch (Bit#(32) x, Bit#(32) y, Bit#(32) z);
  return (x & y)^(~x & z); 
endfunction:ch
  
function Bit#(32) maj (Bit#(32) x, Bit#(32) y, Bit#(32) z);
  return (x & y)^(x & z)^(y & z);
endfunction:maj

Bit#(32) cons_k[64]={
  'h428a2f98, 'h71374491, 'hb5c0fbcf, 'he9b5dba5, 'h3956c25b, 'h59f111f1, 'h923f82a4, 'hab1c5ed5,
  'hd807aa98, 'h12835b01, 'h243185be, 'h550c7dc3, 'h72be5d74, 'h80deb1fe, 'h9bdc06a7, 'hc19bf174,
  'he49b69c1, 'hefbe4786, 'h0fc19dc6, 'h240ca1cc, 'h2de92c6f, 'h4a7484aa, 'h5cb0a9dc, 'h76f988da,
  'h983e5152, 'ha831c66d, 'hb00327c8, 'hbf597fc7, 'hc6e00bf3, 'hd5a79147, 'h06ca6351, 'h14292967,
  'h27b70a85, 'h2e1b2138, 'h4d2c6dfc, 'h53380d13, 'h650a7354, 'h766a0abb, 'h81c2c92e, 'h92722c85,
  'ha2bfe8a1, 'ha81a664b, 'hc24b8b70, 'hc76c51a3, 'hd192e819, 'hd6990624, 'hf40e3585, 'h106aa070,
  'h19a4c116, 'h1e376c08, 'h2748774c, 'h34b0bcb5, 'h391c0cb3, 'h4ed8aa4a, 'h5b9cca4f, 'h682e6ff3,
  'h748f82ee, 'h78a5636f, 'h84c87814, 'h8cc70208, 'h90befffa, 'ha4506ceb, 'hbef9a3f7, 'hc67178f2 };
(* synthesize *)
module mkpipeline_sha_engine(Ifc_sha_engine);
  Integer pre_comp=16;
  Integer a=17;
  Integer b=18;
  Integer c=19;
  Integer d=20;
  Integer e=21;
  Integer f=22;
  Integer g=23;
  Integer h=24;


  Reg#(Bit#(1)) tag[64];
  Reg#(Bit#(32)) buff_data[25][64];
  Reg#(Bit#(32)) initial_hash[8][64];
  
  FIFOF #(Bit#(256)) result_fifo <- mkSizedBRAMFIFOF(`Result_fifo_size);
  
  for(Integer i=0;i<64;i=i+1)
    tag[i]<-mkDReg(0);

  for(Integer i=0;i<25;i=i+1)
    for(Integer j=0;j<64;j=j+1)
      buff_data[i][j]<-mkRegU;
  
  for(Integer i=0;i<8;i=i+1)
    for(Integer j=0;j<64;j=j+1)
      initial_hash[i][j]<-mkRegU;

  rule stage_64(tag[63]==1);
   `ifdef verbose 
      $display("PE:%8h Action:Hash pipeline-stage:%8h",cur_cycle,64);
      $display("a:%8h",buff_data[a][63]);
      $display("b:%8h",buff_data[b][63]);
      $display("c:%8h",buff_data[c][63]);
      $display("d:%8h",buff_data[d][63]);
      $display("e:%8h",buff_data[e][63]);
      $display("f:%8h",buff_data[f][63]);
      $display("g:%8h",buff_data[g][63]);
      $display("h:%8h",buff_data[h][63]);
      $display("w0:%8h",buff_data[0][63]);
      $display("w1:%8h",buff_data[1][63]);
      $display("w2:%8h",buff_data[2][63]);
      $display("w3:%8h",buff_data[3][63]);
      $display("w4:%8h",buff_data[4][63]);
      $display("w5:%8h",buff_data[5][63]);
      $display("w6:%8h",buff_data[6][63]);
      $display("w7:%8h",buff_data[7][63]);
      $display("w8:%8h",buff_data[8][63]);
      $display("w9:%8h",buff_data[9][63]);
      $display("w10:%8h",buff_data[10][63]);
      $display("w11:%8h",buff_data[11][63]);
      $display("w12:%8h",buff_data[12][63]);
      $display("w13:%8h",buff_data[13][63]);
      $display("w14:%8h",buff_data[14][63]);
      $display("w15:%8h",buff_data[15][63]);
      $display("pre-comp:%8h",buff_data[pre_comp][63]);
    `endif
    Bit#(32) tmp1=s1(buff_data[e][63])+ch(buff_data[e][63],buff_data[f][63],buff_data[g][63])+buff_data[pre_comp][63];
    Bit#(32) tmp2=s0(buff_data[a][63])+maj(buff_data[a][63],buff_data[b][63],buff_data[c][63]);
    Bit#(32) tmp_e=buff_data[d][63]+tmp1+initial_hash[4][63];
    Bit#(32) tmp_a=tmp1+tmp2+initial_hash[0][63];
    Bit#(128) tmp_res1={tmp_a,buff_data[a][63]+initial_hash[1][63],buff_data[b][63]+initial_hash[2][63],buff_data[c][63]+initial_hash[3][63]};
    Bit#(128) tmp_res2={tmp_e,buff_data[e][63]+initial_hash[5][63],buff_data[f][63]+initial_hash[6][63],buff_data[g][63]+initial_hash[7][63]};
    result_fifo.enq({tmp_res1,tmp_res2});
    `ifdef verbose $display("PE:%8h Action:Output Digest:%64h",cur_cycle,{tmp_res1,tmp_res2}); `endif
  endrule

  for(Integer i=63;i>0;i=i-1)begin
    rule stage_63to1(tag[i-1]==1);
      `ifdef verbose 
        $display("PE:%8h Action:Hash pipeline-stage:%8h",cur_cycle,i);
        $display("a:%8h",buff_data[a][i-1]);
        $display("b:%8h",buff_data[b][i-1]);
        $display("c:%8h",buff_data[c][i-1]);
        $display("d:%8h",buff_data[d][i-1]);
        $display("e:%8h",buff_data[e][i-1]);
        $display("f:%8h",buff_data[f][i-1]);
        $display("g:%8h",buff_data[g][i-1]);
        $display("h:%8h",buff_data[h][i-1]);
        $display("w0:%8h",buff_data[0][i-1]);
        $display("w1:%8h",buff_data[1][i-1]);
        $display("w2:%8h",buff_data[2][i-1]);
        $display("w3:%8h",buff_data[3][i-1]);
        $display("w4:%8h",buff_data[4][i-1]);
        $display("w5:%8h",buff_data[5][i-1]);
        $display("w6:%8h",buff_data[6][i-1]);
        $display("w7:%8h",buff_data[7][i-1]);
        $display("w8:%8h",buff_data[8][i-1]);
        $display("w9:%8h",buff_data[9][i-1]);
        $display("w10:%8h",buff_data[10][i-1]);
        $display("w11:%8h",buff_data[11][i-1]);
        $display("w12:%8h",buff_data[12][i-1]);
        $display("w13:%8h",buff_data[13][i-1]);
        $display("w14:%8h",buff_data[14][i-1]);
        $display("w15:%8h",buff_data[15][i-1]);
        $display("pre-comp:%8h",buff_data[pre_comp][i-1]);
      `endif
      Bit#(32) tmp1=s1(buff_data[e][i-1])+ch(buff_data[e][i-1],buff_data[f][i-1],buff_data[g][i-1])+buff_data[pre_comp][i-1];
      Bit#(32) tmp2=s0(buff_data[a][i-1])+maj(buff_data[a][i-1],buff_data[b][i-1],buff_data[c][i-1]);
      Bit#(32) tmp_e=buff_data[d][i-1]+tmp1;
      Bit#(32) tmp_a=tmp1+tmp2;
      buff_data[pre_comp][i]<=buff_data[g][i-1]+buff_data[1][i-1]+cons_k[i];
      buff_data[a][i]<=tmp_a;
      buff_data[b][i]<=buff_data[a][i-1];
      buff_data[c][i]<=buff_data[b][i-1];
      buff_data[d][i]<=buff_data[c][i-1];
      buff_data[e][i]<=tmp_e;
      buff_data[f][i]<=buff_data[e][i-1];
      buff_data[g][i]<=buff_data[f][i-1];
      buff_data[h][i]<=buff_data[g][i-1];
      for(Integer j=0;j<15;j=j+1)
        buff_data[j][i]<=buff_data[j+1][i-1];
      buff_data[15][i]<=buff_data[0][i-1]+buff_data[9][i-1]+c0(buff_data[1][i-1])+c1(buff_data[14][i-1]);
      for(Integer j=0;j<8;j=j+1)
        initial_hash[j][i]<=initial_hash[j][i-1];
      tag[i]<=1;
    endrule
  end
  method Action reset;
    for(Integer i=0;i<64;i=i+1)
      tag[i]<=0;
    result_fifo.clear;
  endmethod
  method Action input_engine(Bit#(256) pre_hash,Bit#(512) input_val);
    tag[0]<=1;
    for(Integer i=0;i<16;i=i+1)
      buff_data[i][0]<=input_val[512-(i*32)-1:480-(i*32)];
    buff_data[pre_comp][0]<=input_val[511:480]+pre_hash[31:0]+cons_k[0];
    buff_data[a][0]<=pre_hash[255:224];
    buff_data[b][0]<=pre_hash[223:192];
    buff_data[c][0]<=pre_hash[191:160];
    buff_data[d][0]<=pre_hash[159:128];
    buff_data[e][0]<=pre_hash[127:96];
    buff_data[f][0]<=pre_hash[95:64];
    buff_data[g][0]<=pre_hash[63:32];
    buff_data[h][0]<=pre_hash[31:0];
    initial_hash[0][0]<=pre_hash[255:224];
    initial_hash[1][0]<=pre_hash[223:192];
    initial_hash[2][0]<=pre_hash[191:160];
    initial_hash[3][0]<=pre_hash[159:128];
    initial_hash[4][0]<=pre_hash[127:96];
    initial_hash[5][0]<=pre_hash[95:64];
    initial_hash[6][0]<=pre_hash[63:32];
    initial_hash[7][0]<=pre_hash[31:0];
    `ifdef verbose $display("PE:%8h Action:Input Ph:%64h M:%128h",cur_cycle,pre_hash,input_val); `endif
  endmethod
  interface Get output_engine;
    method ActionValue #(Bit#(256)) get;
      let x=result_fifo.first;
      result_fifo.deq;
      return x;
    endmethod
  endinterface
  method Bool ready;
    return True;
  endmethod
endmodule:mkpipeline_sha_engine
endpackage:pipeline_sha_engine